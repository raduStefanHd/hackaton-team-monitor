var url;
var scrollPosition;

function testConnection() {
    $.ajax({
        url: 'http://raspberrypi.local:5001/pullrequests',
        dataType: 'json',
        type: 'get',
        cache: true,
        success: function () {
            url = 'http://raspberrypi.local:5001/pullrequests';
        },
        error: function() {
            url = 'resources/data.json';
            console.log('Switching to local data');
        }
    });
}

$( document ).ready(function(){
    testConnection();
    ajax();
});

setInterval(function(){
    testConnection();
    ajax();
}, 10000);

function pulsate() {
    var target = $(".comment");
    target.toggle("pulsate", {times: 1}, 2000, function() {
        target.css("opacity", "1");
        target.css("display", "block");
    });
}

setInterval(function(){
    if(scrollPosition) {
        $(".d4").stop().animate({scrollTop:1000}, 2000, 'swing', function() {
            scrollPosition = false;
        });
    } else {
        $(".d4").stop().animate({scrollTop:0}, 2000, 'swing', function() {
            scrollPosition = true;
           pulsate();
        });
    }
},5000);


function ajax() {
     $.ajax({
        url: url,
        dataType: 'json',
        type: 'get',
        cache: true,
        success: function (data) {
            $('#list-items').empty();
            getRequests(data)
        },
        error: function() {
            console.log('error');
        }
    });
}

function getRequests(data) {
    $(data).each(function (index, value) {
        var status;
        switch(value.state) {
            case 'MERGED':
                status = 'green';
                break;
            case 'OPEN':
                status = 'yellow';
                break;
            case 'DECLINED': case 'SUPPRESSED':
                status = 'red';
                break;
        }
        var background;
        switch(value.state) {
            case 'MERGED':
                background = 'background-green';
                break;
            case 'OPEN':
                background = 'background-yellow';
                break;
            case 'DECLINED': case 'SUPPRESSED':
                background = 'background-red';
                break;
        }
        var avatar;
        switch (value.avatar) {
            case 'null':
                avatar = 'resources/96.png';
                break;
            default: avatar = value.avatar;
        }

        if(value.comments_count > 0 && value.state === 'OPEN') {
            var requestData = '<li class="d5 comment list-group-item ' + background + '">';
        } else {
            requestData = '<li class="d5 list-group-item ' + background + '">';
        }
        requestData += '<div class="row">';
        requestData += '<div class="col-xs-3 text-left middle-text padding-left"> <a><img src=' + avatar + ' aria-hidden="true">';
        requestData += '</img><span class="white-text">' + value.author + '</span></a> </div>';
        requestData += '<div class="col-xs-2 middle-text"><div class="branch">' + value.source + '</div></div>';
        requestData += '<div class="col-xs-1 middle-text padding"><span class="glyphicon glyphicon-arrow-right"></span></div>';
        requestData += '<div class="col-xs-2 middle-text"><div class="branch">' + value.destination + '</div></div>';
        requestData += '<div class="col-xs-2 middle-text comment white-text">' +
            '<marquee behavior=scroll direction="left" scrollamount="3">' + value.title + '</marquee>' +
            '</div>';
        if (value.comments_count === 0) {
            requestData += '<a href="#" class="col-xs-2 middle-text padding ' + status + '">' + value.state + '</a>';
        } else {
            requestData += '<a href="#" class="col-xs-2 middle-text padding ' + status + '">' + value.state + ' <span class="badge">' + value.comments_count + '</span></a>';
        }
        requestData += '</div>';
        requestData += '</li>';
        $('#list-items').append(requestData);
    });
    pulsate();
}
